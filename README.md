#Tutorial de instalação do projeto

**Version 1.0.0**

Documentação básica para a clonagem e a instalação do projeto.

Caso você tenha o NPM instalado, não é necessário seguir o passo abaixo. Pode pular para a instalação.

INSTALANDO O NODE
- Entre neste site, baixe e instale o node. Logo após, siga o passo de instalação.
  
https://nodejs.org/en/download/
  
INSTALAÇÃO
```
1) Clone o projeto na sua maquina
2) Entre na pasta raiz
3) instale as dependências utilizando o Node Package Manager (NPM): npm install

```


## License & copyright
Develop by: Thiago Oliveira
