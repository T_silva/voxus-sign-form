$(document).ready(function () {

    var is_validate = false

    $('#form').submit((e) => {
        e.preventDefault()
        const name = $('input[name="name"]')
        const phone = $('input[name="phone"]')
        const email = $('input[name="email"]')
        const company = $('input[name="company"]')
        const website = $('input[name="website"]')
        const money_midia = $('select[name="money_midia"]')

        onValidateForm(name)
        onValidateForm(phone)
        onValidateForm(email)
        onValidateForm(company)
        onValidateForm(website)
        checkUrl(website)
        onValidateForm(money_midia)
        checkEmail(email)



        submitForm(is_validate)
    })

    function onValidateForm(input) {

        if (input.val() !== null) {

            input.parent().removeClass('is_invalid')
            //validate name's field
            if (input.val().length <= 0) {
                is_validate = false
                input.parent().parent().addClass('is_invalid')
                input.parent().parent().children('.error').html(`<span>Este campo é obrigatório!</span>`)
            } else {
                is_validate = true
                input.parent().parent().removeClass('is_invalid')
            }
        } else {
            is_validate = false
            input.parent().addClass('is_invalid')
            input.parent().children('.error').html('<span>Este campo é obrigatório!</span>')
        }
    }

    function checkEmail(email) {
        const email_is_valid = /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email.val())

        if (!email_is_valid) {
            is_validate = false
            email.parent().parent().addClass('is_invalid')
            email.parent().parent().children('.error').html(`<span>E-mail inválido.</span>`)
        } else {
            is_validate = true
            email.parent().parent().removeClass('is_invalid')
        }
    }

    function checkUrl(url) {
        const regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/
        const is_url = regex.test(url.val())

        if (!is_url) {
            is_validate = false
            url.parent().parent().addClass('is_invalid')
            url.parent().parent().children('.error').html(`<span>Domínio inválido.</span>`)
        } else {
            is_validate = true
            url.parent().parent().removeClass('is_invalid')
        }
    }

    function submitForm(is_validate) {
        /** Nessa função, o front faria um Load para o php (Back) - Neste caso vou simular um envio de formulário com êxito */

        if (is_validate) {

            const $btn = $('button[type="submit"]')
            const $message = $('.message-box')

            $btn.hide().addClass('submited').html("Obrigado!").fadeIn('slow')
            $btn.attr('disabled', 'disabled')
            $message.fadeIn('showup')


        } else { }
    }

    /** Select Field - Phone Code */

    const $code_toggle = $('#in-code-select-toggle')
    const $code_list = $('#in-code-select-list')
    const $code_list_item = $('.in-code-select-list-item')
    const $input_phone_code = $('input[name="phone_code"]')

    $code_toggle.click((e) => {
        e.preventDefault()

        $code_list.toggleClass('active')

    })

    /** Close List - Phone Code */
    $(document).mouseup(e => {
        $code_list.removeClass('active')
    });

    /** Select item - Phone Code */
    $code_list_item.click(function (e) {
        e.preventDefault()

        $input_phone_code.val($(this).data('country'))
    })

    /**  Mask Inputs */
    $('input[name="phone"]').mask('(00) 00000-0000')

});