var gulp = require('gulp')
var sass = require('gulp-sass')
var watch = require('gulp-watch');

gulp.task('sass', function () {
    return gulp.src('assets/sass/**/*.scss')
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(gulp.dest('dist/css'));
});

//task para o watch
gulp.task('watch', function () {
    gulp.watch('assets/sass/**/*.scss', gulp.series('sass'));
});

gulp.task('default', gulp.parallel('sass'));